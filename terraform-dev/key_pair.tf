resource "aws_key_pair" "microservices_keypair" {
  key_name   = "microservices-keypair"

  public_key = file("~/.ssh/id_rsa.pub")
}