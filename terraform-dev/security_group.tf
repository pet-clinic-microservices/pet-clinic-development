module "security-group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "5.1.0"

  name        = "dev-server-sg"
  description = "This security group is used for Dev server."

  # ingress_cidr_blocks = ["0.0.0.0/0"]

  ingress_with_cidr_blocks = [
    {
      from_port       = "22"
      to_port         = "22"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "80"
      to_port         = "80"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "8080"
      to_port         = "8080"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "8081"
      to_port         = "8081"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "8082"
      to_port         = "8082"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "8083"
      to_port         = "8083"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "9090"
      to_port         = "9090"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "9411"
      to_port         = "9411"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "7979"
      to_port         = "7979"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "3000"
      to_port         = "3000"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "9091"
      to_port         = "9091"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "8761"
      to_port         = "8761"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

    {
      from_port       = "8888"
      to_port         = "8888"
      protocol        = "tcp"
      cidr_blocks     = "0.0.0.0/0"
    },

  ]


  # egress_rules       = ["all-all"] 
  # egress_cidr_blocks = [ "0.0.0.0/0" ]
  egress_with_cidr_blocks = [
    {
      from_port       = "-1"
      to_port         = "-1"
      protocol        = "-1"
      cidr_blocks     = "0.0.0.0/0"
    },
  ]
}

# -------------------------------------------------
# resource "aws_security_group" "test_sg" {
#   name        = "test-sg"

# dynamic "ingress" {
#     for_each = [22, 80, 443, 3306]

#     content {
#       from_port   = ingress.value
#       to_port     = ingress.value
#       protocol    = "tcp"
#       cidr_blocks = ["0.0.0.0/0"]
#     }
#   }

# }