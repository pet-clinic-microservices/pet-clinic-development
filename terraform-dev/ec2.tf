resource "aws_instance" "pet-clinic-dev-server" {
  ami             = "ami-0905a3c97561e0b69"
 
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.microservices_keypair.key_name

  vpc_security_group_ids      = ["${module.security-group.security_group_id}"]

  associate_public_ip_address = true
  tags = {
    Name          = "pet-clinic-web-server"
    Project       = "pet-clinic"
    Environment   = "development"
    Role          = "web-server"
    Tier          = "frontend, backend"
  }

  user_data = <<-EOF
              #!/bin/bash
              sudo apt update -y
              sudo hostnamectl set-hostname petclinic-dev-server

              sudo install -m 0755 -d /etc/apt/keyrings
              curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
              sudo chmod a+r /etc/apt/keyrings/docker.gpg

              echo \
                "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
                "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
                sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

              sudo apt update -y

              sudo apt install docker.io
              
              sudo systemctl start docker
              sudo systemctl enable docker
              sudo usermod -aG docker $USER

              sudo apt-get install git -y 2> /dev/null
              sudo apt-get install openjdk-11-jdk -y
              newgrp docker
              EOF
}
